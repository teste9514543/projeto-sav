# Projeto de Ordenação Avançada

Bem-vindo ao Projeto de Ordenação Avançada! Este é um sistema de ordenação em Java que permite aos usuários escolher entre diferentes algoritmos de ordenação, tipos de dados, e mais, tudo através de uma interface de linha de comando amigável. Ideal para fins educativos e para visualizar como diferentes algoritmos de ordenação manipulam conjuntos de dados.

# Link de acesso ao vídeo

https://youtu.be/wQx-4obtj5U?si=mMX2MQCH-alU88PH

## Recursos

- **Vários Algoritmos de Ordenação**: Escolha entre Bubble Sort, Insertion Sort e Selection Sort.
- **Tipos de Dados Suportados**: Ordenação de números inteiros e caracteres.
- **Fontes de Dados Flexíveis**: Dados podem ser fornecidos manualmente pelo usuário ou gerados aleatoriamente pelo sistema.
- **Ordenação Ascendente ou Descendente**: Especifique a direção da ordenação.
- **Visualização do Processo de Ordenação**: Acompanhe o processo de ordenação com um delay configurável entre as operações.

## Como Usar

1. **Configurar o Ambiente**: Certifique-se de ter o Java instalado e configurado em seu sistema.

2. **Executar o Programa**: Abra o terminal e navegue até a pasta contendo o código fonte. Compile e execute o programa com os comandos:

    ```bash
    javac SortingApplication.java
    java SortingApplication
    ```

3. **Passar Opções via Linha de Comando**: O programa pode ser configurado via linha de comando usando várias opções. Aqui estão um exemplo de como você pode passar argumentos para o programa:

    ```bash
    java SortingApplication --sort=bubble --ascending=true --dataType=number --source=random --values="1,2,3,4,5" --random-count=10
    ```

    - `--sort`: Especifica o algoritmo de ordenação (ex: `bubble`, `insertion`, `selection`).
    - `--ascending`: Define se a ordenação será ascendente (`true`) ou descendente (`false`).
    - `--dataType`: Define o tipo de dados (`number` ou `character`).
    - `--source`: Especifica a fonte dos dados (`random` para dados gerados aleatoriamente ou `manual` para dados inseridos manualmente).
    - `--values`: Fornece os valores a serem ordenados quando a fonte é manual, separados por vírgulas (ex: `"1,2,3,4,5"`).
    - `--random-count`: Especifica o número de valores a serem gerados aleatoriamente se a fonte for `random`.

4. **Visualize o Resultado**: Após a ordenação, a lista ordenada será exibida no terminal, juntamente com o tempo de ordenação.


## Diagrama de Classe

O diagrama de classe abaixo ilustra a estrutura do projeto, incluindo classes, interfaces e enumerações, bem como suas relações.

![Diagrama de Classe](https://gitlab.com/teste9514543/projeto-sav/-/raw/main/Sav/Diagrama%20de%20classe/Diagrama%20de%20classe%20Sav.png?ref_type=heads)
