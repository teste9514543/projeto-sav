import java.util.List;
/**
 * Implementação do algoritmo de ordenação Bubble Sort.
 * Compara pares adjacentes e troca suas posições se estiverem na ordem errada.
 */
public class BubbleSort implements SortingAlgorithm {
    /**
     * Tempo de atraso em milissegundos entre as operações de troca para visualização.
     */
    private final long delayTime;
    /**
     * Constrói uma instância de BubbleSort com um tempo de atraso específico.
     *
     * @param delayTime Tempo de atraso em milissegundos.
     */
    public BubbleSort(long delayTime) {
        this.delayTime = delayTime;
    }

    @Override
    public <T extends Comparable<T>> void sort(List<T> list, boolean ascending) throws InterruptedException {
        int iterationCount = 0;
        boolean swapped;
        for (int i = 0; i < list.size(); i++) {
            swapped = false;
            for (int j = 1; j < list.size() - i; j++) {
                if ((ascending && list.get(j).compareTo(list.get(j - 1)) < 0) ||
                        (!ascending && list.get(j).compareTo(list.get(j - 1)) > 0)) {
                    T temp = list.get(j);
                    list.set(j, list.get(j - 1));
                    list.set(j - 1, temp);
                    swapped = true;
                    System.out.println("Iteração " + (++iterationCount) + ": " + list);

                    Thread.sleep(this.delayTime);
                }
            }
            if (!swapped) {
                break;
            }
        }
    }
}

