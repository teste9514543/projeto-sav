import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * Aplicativo CLI para interação com o usuário e execução dos algoritmos de ordenação.
 * Permite a configuração de algoritmos de ordenação, tipo de dados, e mais.
 */
public class CLIApplication {
    /**
     * Ponto de entrada principal do aplicativo CLI.
     *
     * @param args Argumentos da linha de comando.
     */
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Bem-vindo ao Sistema de Ordenação Avançada!\n");

        System.out.print("Digite o tempo de delay em milissegundos (entre 100 e 1000): ");
        long delay = scanner.nextLong();
        delay = Math.max(100, Math.min(delay, 1000));

        SortingFactory.setDelayTime(delay);

        SortingAlgorithm algorithm = selectSortingAlgorithm();

        boolean ascending = chooseOrder();

        DataType dataType = chooseDataType();

        ValueSource valueSource = chooseValueSource();

        List<Comparable> data;
        if (valueSource == ValueSource.RANDOM) {
            data = generateRandomData(dataType);
        } else {
            data = parseManualData(dataType);
        }

        System.out.println("Lista inicial: " + data);
        long startTime = System.nanoTime();
        try {
            algorithm.sort(data, ascending);
        } catch (InterruptedException e) {
            System.err.println("Erro durante a ordenação: " + e.getMessage());
            Thread.currentThread().interrupt();
        }
        long endTime = System.nanoTime();

        long duration = (endTime - startTime) / 1_000_000;

        System.out.println("Lista ordenada: " + data);
        System.out.println("Tempo de ordenação: " + duration + " ms");
    }

    private static SortingAlgorithm selectSortingAlgorithm() {
        System.out.println("Selecione o algoritmo de ordenação:");
        System.out.println("1. Bubble Sort");
        System.out.println("2. Insertion Sort");
        System.out.println("3. Selection Sort");

        int choice = scanner.nextInt();
        SortAlgorithm userSelectedAlgorithm = SortAlgorithm.values()[choice - 1];
        return SortingFactory.createAlgorithm(userSelectedAlgorithm);
    }

    private static boolean chooseOrder() {
        System.out.println("Escolha a ordem da ordenação:\n1. Ascendente\n2. Descendente");
        int choice = scanner.nextInt();
        return choice == 1;
    }

    private static DataType chooseDataType() {
        System.out.println("Escolha o tipo de dados:\n1. Números\n2. Caracteres");
        int choice = scanner.nextInt();
        return choice == 1 ? DataType.NUMBER : DataType.CHARACTER;
    }

    private static ValueSource chooseValueSource() {
        System.out.println("Escolha a fonte dos dados:\n1. Manualmente\n2. Aleatoriamente");
        int choice = scanner.nextInt();
        return choice == 1 ? ValueSource.MANUAL : ValueSource.RANDOM;
    }

    private static List<Comparable> generateRandomData(DataType dataType) {
        List<Comparable> data = new ArrayList<>();
        System.out.println("Quantos valores aleatórios deseja gerar?");
        int numValues = scanner.nextInt();
        Random rand = new Random();

        if (dataType == DataType.NUMBER) {
            for (int i = 0; i < numValues; i++) {
                data.add(rand.nextInt(2001) - 1000); // Números entre -1000 e 1000
            }
        } else {
            for (int i = 0; i < numValues; i++) {
                data.add((char) (rand.nextInt(26) + 'a')); // Caracteres de 'a' a 'z'
            }
        }
        return data;
    }

    private static List<Comparable> parseManualData(DataType dataType) {
        System.out.println("Digite os valores separados por vírgula:");
        scanner.nextLine(); // Consume newline left-over
        String input = scanner.nextLine();
        List<Comparable> data = new ArrayList<>();
        if (dataType == DataType.NUMBER) {
            for (String value : input.split(",")) {
                data.add(Integer.parseInt(value.trim()));
            }
        } else {
            for (String value : input.split(",")) {
                data.add(value.trim().charAt(0));
            }
        }
        return data;
    }

    private static long getDelayTime() {
        System.out.println("Digite o tempo de delay em milissegundos (entre 100 e 1000):");
        long delayTime = scanner.nextLong();
        return Math.max(100, Math.min(delayTime, 1000)); // Assegura que o valor esteja dentro do intervalo permitido
    }
}
