/**
 * Armazena as opções de configuração extraídas dos argumentos da linha de comando.
 */
public class CommandLineOptions {

    /**
     * Define o algoritmo de ordenação selecionado.
     *
     * @param selectedSortAlgorithm O algoritmo de ordenação a ser usado.
     */
    private SortAlgorithm selectedSortAlgorithm = SortAlgorithm.BUBBLE;
    private boolean ascending = true;
    private long sortDelayTime = 100;
    private DataType dataType = DataType.NUMBER;
    private ValueSource valueSource = ValueSource.RANDOM;
    private String inputValues = "";
    private int numberOfRandomValues = 10;

    public SortAlgorithm getSelectedSortAlgorithm() {
        return selectedSortAlgorithm;
    }

    public void setSelectedSortAlgorithm(SortAlgorithm selectedSortAlgorithm) {

        this.selectedSortAlgorithm = selectedSortAlgorithm;
    }

    public boolean isAscending() {
        return ascending;
    }

    public void setAscending(boolean ascending) {
        this.ascending = ascending;
    }

    public long getSortDelayTime() {
        return sortDelayTime;
    }

    public void setSortDelayTime(long sortDelayTime) {
        this.sortDelayTime = sortDelayTime;
    }

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public ValueSource getValueSource() {
        return valueSource;
    }

    public void setValueSource(ValueSource valueSource) {
        this.valueSource = valueSource;
    }

    public String getInputValues() {
        return inputValues;
    }

    public void setInputValues(String inputValues) {
        this.inputValues = inputValues;
    }

    public int getNumberOfRandomValues() {
        return numberOfRandomValues;
    }

    public void setNumberOfRandomValues(int numberOfRandomValues) {
        this.numberOfRandomValues = numberOfRandomValues;
    }
}
