/**
 * Analisa os argumentos da linha de comando e configura as opções de ordenação.
 */
public class CommandLineParser {
    /**
     * Analisa os argumentos da linha de comando fornecidos e retorna as opções configuradas.
     *
     * @param args Argumentos da linha de comando.
     * @return As opções de ordenação configuradas.
     */
    public CommandLineOptions parse(String[] args) {
        CommandLineOptions options = new CommandLineOptions();
        for (String arg : args) {
            if (arg.startsWith("--sort=")) {
                options.setSelectedSortAlgorithm(SortAlgorithm.valueOf(arg.substring(7).toUpperCase()));
            } else if (arg.startsWith("--ascending=")) {
                options.setAscending(Boolean.parseBoolean(arg.substring(12)));
            } else if (arg.startsWith("--delay=")) {
                options.setSortDelayTime(Long.parseLong(arg.substring(8)));
            } else if (arg.startsWith("--data-type=")) {
                options.setDataType(DataType.valueOf(arg.substring(12).toUpperCase()));
            } else if (arg.startsWith("--source=")) {
                options.setValueSource(ValueSource.valueOf(arg.substring(9).toUpperCase()));
            } else if (arg.startsWith("--values=")) {
                options.setInputValues(arg.substring(9));
            } else if (arg.startsWith("--random-count=")) {
                options.setNumberOfRandomValues(Integer.parseInt(arg.substring(15)));
            }
        }
        return options;
    }
}
