import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
/**
 * Gera dados para ordenação baseado nas opções fornecidas.
 */
public class DataGenerator {
    /**
     * Gera uma lista de dados comparáveis conforme as opções especificadas.
     *
     * @param options As opções de geração de dados.
     * @return Lista de dados comparáveis.
     */
    public List<Comparable> generateData(CommandLineOptions options) {
        List<Comparable> data = new ArrayList<>();
        Random rand = new Random();

        if (options.getValueSource() == ValueSource.RANDOM) {
            for (int i = 0; i < options.getNumberOfRandomValues(); i++) {
                if (options.getDataType() == DataType.NUMBER) {
                    data.add(rand.nextInt(2001) - 1000);
                } else if (options.getDataType() == DataType.CHARACTER) {
                    data.add((char) (rand.nextInt(26) + 'a'));
                }
            }
        } else if (options.getValueSource() == ValueSource.MANUAL) {
            if (options.getDataType() == DataType.NUMBER) {
                Arrays.stream(options.getInputValues().split(","))
                        .map(Integer::parseInt)
                        .forEach(data::add);
            } else if (options.getDataType() == DataType.CHARACTER) {
                for (String s : options.getInputValues().split(",")) {
                    if (!s.isEmpty()) {
                        data.add(s.charAt(0));
                    }
                }
            }
        }
        return data;
    }
}
