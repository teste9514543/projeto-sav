/**
 * Enumeração dos tipos de dados suportados pela aplicação de ordenação.
 */
public enum DataType {
    /**
     * Representa dados numéricos para ordenação.
     */
    NUMBER, CHARACTER
    /**
     * Representa caracteres alfabéticos para ordenação.
     */
}
