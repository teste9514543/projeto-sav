import java.util.List;
/**
 * Implementa o algoritmo de ordenação por inserção.
 * Ordena uma lista de elementos comparáveis inserindo cada elemento em sua posição correta na parte ordenada da lista.
 */
public class InsertionSort implements SortingAlgorithm {
    private final long delayTime;
    /**
     * Constrói uma instância de InsertionSort com um tempo de atraso específico.
     *
     * @param delayTime Tempo de atraso em milissegundos entre cada operação de inserção para visualização.
     */
    public InsertionSort(long delayTime) {
        this.delayTime = delayTime;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public <T extends Comparable<T>> void sort(List<T> list, boolean ascending) throws InterruptedException {
        int iterationCount = 0;
        for (int i = 1; i < list.size(); i++) {
            T key = list.get(i);
            int j = i - 1;

            while (j >= 0 && (ascending ? list.get(j).compareTo(key) > 0 : list.get(j).compareTo(key) < 0)) {
                list.set(j + 1, list.get(j));
                j--;

                System.out.println("Iteração " + (++iterationCount) + ": " + list);
                Thread.sleep(this.delayTime);
            }
            list.set(j + 1, key);
        }
    }
}

