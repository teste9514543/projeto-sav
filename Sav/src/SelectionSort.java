import java.util.List;
/**
 * Implementa o algoritmo de ordenação por seleção.
 * Ordena uma lista de elementos comparáveis encontrando repetidamente o mínimo (ou máximo) elemento
 * e movendo-o para a sua posição correta na lista.
 */
public class SelectionSort implements SortingAlgorithm {
    private final long delayTime;
    /**
     * Constrói uma instância de SelectionSort com um tempo de atraso específico.
     *
     * @param delayTime Tempo de atraso em milissegundos entre cada operação de seleção para visualização.
     */
    public SelectionSort(long delayTime) {

        this.delayTime = delayTime;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public <T extends Comparable<T>> void sort(List<T> list, boolean ascending) throws InterruptedException {
        int iterationCount = 0;
        for (int i = 0; i < list.size() - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < list.size(); j++) {
                if ((ascending && list.get(j).compareTo(list.get(minIndex)) < 0) ||
                        (!ascending && list.get(j).compareTo(list.get(minIndex)) > 0)) {
                    minIndex = j;
                }
            }
            T temp = list.get(minIndex);
            list.set(minIndex, list.get(i));
            list.set(i, temp);

            System.out.println("Iteração " + (++iterationCount) + ": " + list);
            Thread.sleep(this.delayTime);
        }
    }
}

