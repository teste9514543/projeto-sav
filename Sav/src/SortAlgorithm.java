/**
 * Enumeração dos algoritmos de ordenação disponíveis.
 */
public enum SortAlgorithm {
    BUBBLE, INSERTION, SELECTION
}

