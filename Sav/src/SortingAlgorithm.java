import java.util.List;
/**
 * Interface para algoritmos de ordenação.
 * Define um método para ordenar uma lista de elementos comparáveis.
 */
public interface SortingAlgorithm {
    /**
     * Ordena uma lista de elementos em ordem ascendente ou descendente.
     *
     * @param <T>        Tipo de elementos na lista que estende Comparable<T>.
     * @param list       A lista de elementos a ser ordenada.
     * @param ascending  Define a direção da ordenação. Verdadeiro para ascendente, falso para descendente.
     * @throws InterruptedException Se a execução for interrompida.
     */
    <T extends Comparable<T>> void sort(List<T> list, boolean ascending) throws InterruptedException;
}
