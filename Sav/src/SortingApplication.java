import java.util.List;
/**
 * Classe principal que executa a aplicação de ordenação.
 * Permite a configuração e execução de diferentes algoritmos de ordenação através da linha de comando.
 */
public class SortingApplication {
    /**
     * Ponto de entrada principal da aplicação. Processa os argumentos da linha de comando,
     * configura o ambiente de ordenação com base nas opções fornecidas, e executa o algoritmo de ordenação selecionado.
     *
     * @param args Argumentos da linha de comando para configurar a ordenação.
     */
    public static void main(String[] args) {
        CommandLineParser parser = new CommandLineParser();
        CommandLineOptions options = parser.parse(args);
        DataGenerator generator = new DataGenerator();

        List<Comparable> data = generator.generateData(options);
        SortingAlgorithm algorithm = SortingFactory.createAlgorithm(options.getSelectedSortAlgorithm());

        System.out.println("Initial list: " + data);
        try {
            algorithm.sort(data, options.isAscending());
            System.out.println("Sorted list: " + data);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            System.err.println("Sorting was interrupted.");
        }
    }
}
