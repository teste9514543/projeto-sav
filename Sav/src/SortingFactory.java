/**
 * Cria instâncias de algoritmos de ordenação baseado na seleção do usuário.
 */
public class SortingFactory {
    /**
     * Configura o tempo de atraso global para visualização da ordenação.
     *
     * @param newDelayTime Novo tempo de atraso em milissegundos.
     */
    private static long delayTime = 100;

    public static void setDelayTime(long newDelayTime) {
        delayTime = newDelayTime;
    }
    /**
     * Cria uma instância do algoritmo de ordenação selecionado.
     *
     * @param algorithm O algoritmo de ordenação escolhido.
     * @return Uma instância do algoritmo de ordenação.
     */
    public static SortingAlgorithm createAlgorithm(SortAlgorithm algorithm) {


        switch (algorithm) {
            case BUBBLE:
                return new BubbleSort(delayTime);
            case INSERTION:
                return new InsertionSort(delayTime);
            case SELECTION:
                return new SelectionSort(delayTime);

            default:
                throw new IllegalArgumentException("Unsupported sorting algorithm: " + algorithm);
        }
    }
}
