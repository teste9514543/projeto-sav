/**
 * Enumeração das fontes de valores para ordenação.
 */
public enum ValueSource {
    RANDOM, MANUAL
}
